# GoonTools

GoonTools is a tool used to pull data from robots running around a facility and aggregate
it onto the facility map.  It can also graph error rates per robot.

# Requirements

Docker is required to build and run the local server.  You need permissions to run docker
images and to pull docker images from dockerhub.

# Building
## Linux and Mac

Run `build.sh` in the `ops` directory.  Mac building has not been tested, but it should work.

## Windows

Run `build.bat` in the `ops` directory.

# Run/Serve
## Linux and Mac
To serve the built application under Linux and Mac, run `serve.sh` in the `ops` directory.
Mac serving has not been tested, but it should work.  Open a browser and navigate to
`localhost:8000` in the address bar to view the web application.

## Windows
To serve the built application under Windows, run `serve.bat` in the `ops` directory.
Open a browser and navigate to `localhost:8000` in the address bar to view the web
application.

# Usage
## Homepage
The Homepage can be reached by clicking the `GOONTOOLS` logo in the navbar.  Currently,
the Homepage is just a place holder.

## Events Tab
On the left, the names of all robots discovered are listed in the accordian component.

On the right, the facility map is displayed.  Blue lines are robot segments (routes). Dark
green dots are robot stations.  Red dots (when visible) are locations where a robot has
encountered an error (event code) at some point in time.  The map can be panned and zoomed
with the mouse by clicking the controls or by drag 'n drop and mouse scrolling.

Hovering over a robot name in the accordian component makes all errors that robot has
encountered visible on the map, marked with red dots.

Clicking the robot name in the accordian component expands it to list all of the errors
(events) that robot has encountered with the event code #, the date and time of the event,
and the segment the robot was on when it occurred, listed by the station names that
comprise the segment.  Hovering over an event code will isolate only that error on the
map, and will show a preview of what the HMI and text of the event code was
on the robot on the right of the map.

Hovering over a dot on the map will give more information in a tooltip.  Hovering over a
station (dark green dots) gives the name of the station.  Hovering over a event (red dots)
gives the event code #, which robot had the event, and the date/time of the event.

The Filter input allows to filter on any type of data that matches the filter with
wild-cards.  You can filter by robot names (even partial names), event codes (prefix
with #), date/time, station name, etc.  The filtering also affects the accordian component
on the left.  As an example, you can type `VGV02` to see only event codes that VGV02 has
encountered, `Fri` to see event codes that only occurred on a Friday, `733` to see event
codes associated with station 733, `#15` to only see event codes #15, and you can combine
them by typing `VGV02 Fri 733 #15` to see only event codes #15 encountered by VGV02 on a
Friday in association with station 733.

## Facility Map Tab
The facility map is a larger map component with additional details that can be viewed.
You can view map data by Events, Obstructions, and Wi-Fi using the radio buttons on the
left.

Events work the same as the map view from the Events Tab, minus the accordian component.

Obstructions are locations where the robot became obstructed at some point in time.
Obstructions appear as yellow dots on the map.  Hovering over the yellow dot gives more
information about the obstruction -- which robot encountered the obstruction, the length
of time of the obstruction, and the date/time of the obstruction.  This can be useful for
determining where human operators are consistently leaving objects in the robot's path,
which will negatively affect TACT time.

The Wi-Fi view shows the worst reported wifi strength from any robot for a given part of
each segment for which a robot has traversed.  The color of the dot ranges from green to
red, corresponding with wifi strength.  Solid green is 75% and up, and solid red is 25%
and below.  Hovering over the dots gives the wifi strength in a tooltip.  In the sample
data, you can see an aisle on the right side of the map that suffers from very poor wifi
coverage.  You can also see there are several segments on the top left corner of the map
that no robots have traversed during the data collection period.

## Charts Tab
A histogram of events experienced by each robot can be viewed on the Charts Tab.

The sample data here was collected from a different facility that had a lot more robots.

You can view a Daily chart, Weekly chart, or Monthly chart.

The time-span component (blue line above or below the chart with arrows on either side)
allows you to narrow the period of time, which could be useful if you just wanted to see
data over a specific 5 day period on the Daily chart, for example, or wanted to compare
one week to another week on the Weekly chart, instead of the default of seeing up to 4
weeks at a time.  There is a known bug where the time-span component toggles from above
the graph to below the graph every time you interact with it.

You can filter robots with the Filter VGVs input box on the left.  This will wild-card
match only robot names with a space-separated list.  Clicking the Excludes VGVs checkbox
will make matches exclusionary instead of inclusionary.  For the example data, all of the
even-numbered robots are tuggers, and all of the odd-numbered robots are pallet jacks, so
if you were only interested in one or the other, you could type `02 04 06 08 10 12` to
show only tugger data, or click the Excludes VGVs checkbox to show only pallet jack data.

You can filter events with the Filter Events input box.  It's not necessary to prefix the
event code with `#`.  Again, you can toggle the Excludes Events checkbox to make the
filter exclusionary instead of inclusionary.

Clicking the Filter User Events button will fill the Filter Events input box with event
codes that are typically caused by operator error or intervention, and clicking the
Filter System Events button will fill the Filter Events input box with the same codes,
but will also check the Excludes Events checkbox, causing it to be the inverse.

Hovering over any bar in the chart will give display the number of events that each robot
encountered for the given time period.

## Event Codes Tab
Clicking the Event Codes Tab will bring up a bottom drawer component that has all of the
possible event codes in a table.  Clicking an Event Code in the drawer will take you to a
page showing a preview of what the event code will look like on the HMI of the robot.
Troubleshooting tips or flowcharts could be added to these pages in the future.

## Symptoms Tab
Clicking the Symptoms Tab will bring up a bottom drawer component that has some common
symptoms that a technician may encounter.  Clicking the symptoms will bring you to a
placeholder page where troubleshooting tips or flowcharts could be placed in the future.
