#!/bin/bash

SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

docker build -t goontools -f "$SCRIPT_PATH/goontools.docker" "$SCRIPT_PATH"

docker run --rm -it \
    -v "$SCRIPT_PATH/../src/frontend":/app \
    goontools

