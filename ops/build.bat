set SCRIPT_PATH=%cd%

docker build -t goontools -f "%SCRIPT_PATH%\goontools.docker" "%SCRIPT_PATH%"

docker run --rm -it -v "%SCRIPT_PATH%/../src/frontend":/app goontools
