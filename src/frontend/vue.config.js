module.exports = {
    configureWebpack: {
        devtool: 'source-map',
        module: {
            rules: [
                {
                    test: /\.js$/,
                    loader: 'ify-loader'
                }
            ]
        }
    }
};
