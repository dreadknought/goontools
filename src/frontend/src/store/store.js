import 'es6-promise/auto';
import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        inputFilter: [], // Holds the text from the navbar input
        mapFilter: ['__'], // Holds our desired map filter
        showInputFilter: true, // Shows/hides the input filter in the navbar
        eventNumber: null, // The event number to show on the Events view
        snack: {
            // Snackbar object
            snack: '',
            timeout: 2000
        }
    },
    mutations: {
        setSnack(state, snack) {
            /*  @brief  Shows the snackbar
                @param  snack The message to show in the snackbar
            */
            state.snack.snack = snack;
        },
        setEventNumber(state, eventNumber) {
            /*  @brief  Changes the eventNumber to show in events view
                @param  eventNumber The event number to show.  null
                        hides it.
            */
            state.eventNumber = eventNumber;
        },
        setMapFilter(state, filter) {
            /*  @brief  Filters the map data points
                @param  filter A space-seperated string or array of strings
                        to filter the map data points with
            */
            if (filter) {
                if (typeof filter === 'string') {
                    filter = filter.split(' ');
                } else if (Array.isArray(filter)) {
                    filter = filter.join(' ').split(' '); // Tokenizes the entire array by spaces
                }
            } else {
                filter = [''];
            }
            state.mapFilter = filter;
        },
        setInputFilter(state, filter) {
            /*  @brief  Allows global access to the input filter.  Also updates the map filter
                        at the same time.
                @param filter A space-separated string or array of strings
            */
            if (filter) {
                if (typeof filter === 'string') {
                    filter = filter.split(' ').filter(function(val) {
                        return val != '';
                    });
                } else if (Array.isArray(filter)) {
                    filter = filter
                        .join(' ')
                        .split(' ')
                        .filter(function(val) {
                            return val != '';
                        }); // Tokenizes the entire array by spaces
                }
            } else {
                filter = [''];
            }
            state.inputFilter = filter;
            this.commit('setMapFilter', filter);
        },
        showFilterInput(state) {
            // Show the filter input in the navbar
            state.showInputFilter = true;
        },
        hideFilterInput(state) {
            // Hide the filter input in the navbar
            state.showInputFilter = false;
        }
    }
});
