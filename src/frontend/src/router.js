import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/events',
            name: 'events',
            component: () =>
                import(/* webpackChunkName: 'events' */ './views/Events.vue') // Lazy-load Events view
        },
        {
            path: '/facilitymap',
            name: 'facilitymap',
            component: () =>
                import(/* webpackChunkName: 'facilitymap' */ './views/FacilityMap.vue') // Lazy-load Facility Map view
        },
        {
            path: '/eventcodes/:eventNumber',
            name: 'eventcodes',
            component: () =>
                import(/*webpackChunkName: 'eventcodes' */ './views/EventCodes.vue') // Lazy-load Event Codes view
        },
        {
            path: '/symptoms/:symptom',
            name: 'symptoms',
            component: () =>
                import(/*webpackChunkName: 'symptoms' */ './views/Symptoms.vue') // Lazy-load Symptoms view
        },
        {
            path: '/charts',
            name: 'charts',
            component: () =>
                import(/*webpackChunkName: 'charts' */ './views/Charts.vue') // Lazy-load
        }
    ]
});
